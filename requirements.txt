# Core
pytz==2021.1
Django==3.2.4
django-configurations==2.2
django-money[exchange]==2.0
requests==2.25.1 

# For the persistence stores
psycopg2-binary==2.8.6
dj-database-url==0.5.0

# Model Tools
django-model-utils==4.1.1
django_unique_upload==0.2.1

# Rest apis
djangorestframework==3.12.4
Markdown==3.3.4
django-filter==2.4.0

# Developer Tools
ipdb==0.13.9
ipython==7.24.1
mkdocs==1.1.2
flake8==3.9.2
pudb==2021.1

# Testing
mock==4.0.3
factory-boy==3.2.0
django-nose==1.4.7
nose-progressive==1.5.2
coverage==5.5
pytest==6.2.4
