# Oders
Supports creating and viewing of BTC orders

## Create a new order

**Request**:

`POST` `/orders/`

Parameters:

Name              | Type   | Required | Description
------------------|--------|----------|------------
amount            | string | Yes      | Fiat currency value. (String of int or float with max 8 decimal places)
amount_currency   | string | Yes      | Fiat currency. (Only valid choices: "EUR", "USD", "GBP")


*Note:*

- `X-Idempotency-Key` header needs to be sent with this request
- The value of this header needs to be unique per request can be randomly
  generated with python:

```bash
python3

import uuid

str(uuid.uuid4())

```
- There is a cache timeout set for `60` seconds so for testing purposes, one can
  also wait `60` seconds before sending the same used `X-Idempotency-Key`
  Header value for a successful create order response
- cache timeout can also be set to another value in the `.env` file (`IDEMPOTENCY_TIMEOUT`)

**Response**:

```json
Content-Type application/json
201 Created

{
    "id": "38cd31b7-6519-4950-b08f-7c60402184ca",
    "created_at": "2021-06-11T17:12:07+0000",
    "amount_currency": "USD",
    "amount": "100.00000000",
    "exchange_rate": "36766.93670000",
    "btc_amount_currency": "BTC",
    "btc_amount": "0.00271983"
}
```

For CURL testing examples See **[Documentation Gitlab](https://gitlab.com/amaralunao/xapo-assignment/-/blob/master/docs/index.md)**.


## Get a list of created orders

**Request**:

`GET` `/list-orders/`

Parameters:

*Note:*

- The response is paged and there are a maximum of `10` entries per page (setting)
- To access the next page: `GET` `/list-orders/?page=2`
- There is a database index on `created_at` field for better response time.

**Response**:

```json
Content-Type application/json
200 OK

{
    "count": 31,
    "next": "http://0.0.0.0:8000/api/v1/list-orders/?page=2",
    "previous": null,
    "results": [
        {
            "id": "f14d3e72-b9af-4fbe-a7b5-9a8a08f78090",
            "created_at": "2021-06-10T12:56:14+0000",
            "amount_currency": "EUR",
            "amount": "100.00000000",
            "exchange_rate": "31036.22290000",
            "btc_amount_currency": "BTC",
            "btc_amount": "0.00322204"
        },
        {
            "id": "f492994c-332a-40d3-a614-264f2c63c3ce",
            "created_at": "2021-06-10T11:36:54+0000",
            "amount_currency": "EUR",
            "amount": "100.00000000",
            "exchange_rate": "31050.92570000",
            "btc_amount_currency": "BTC",
            "btc_amount": "0.00322052"
        },
        {
            "id": "3b3cef52-f0d5-4290-9d4a-22b34be13a22",
            "created_at": "2021-06-10T11:01:29+0000",
            "amount_currency": "EUR",
            "amount": "100.00000000",
            "exchange_rate": "31202.12190000",
            "btc_amount_currency": "BTC",
            "btc_amount": "0.00320491"
        },
        {
            "id": "74af71e9-1852-4fea-8fb1-e2410fda0308",
            "created_at": "2021-06-10T10:15:30+0000",
            "amount_currency": "EUR",
            "amount": "10000.00000000",
            "exchange_rate": "31339.15050000",
            "btc_amount_currency": "BTC",
            "btc_amount": "0.31908970"
        },
        {
            "id": "db2eb8e9-65fb-45ae-817d-4cf9cb659326",
            "created_at": "2021-06-10T10:15:20+0000",
            "amount_currency": "EUR",
            "amount": "10000.00000000",
            "exchange_rate": "31339.15050000",
            "btc_amount_currency": "BTC",
            "btc_amount": "0.31908970"
        },
        {
            "id": "d4178a2e-170b-4e13-aadf-ede666e6f54f",
            "created_at": "2021-06-10T10:12:23+0000",
            "amount_currency": "EUR",
            "amount": "1000000000.00000000",
            "exchange_rate": "31356.23880000",
            "btc_amount_currency": "BTC",
            "btc_amount": "31891.58005775"
        },
        {
            "id": "0b41381d-d898-46fe-aa83-4da311848adf",
            "created_at": "2021-06-10T09:11:02+0000",
            "amount_currency": "EUR",
            "amount": "100.00000000",
            "exchange_rate": "29904.69590000",
            "btc_amount_currency": "BTC",
            "btc_amount": "0.00334396"
        },
        {
            "id": "ec6c89ab-55bb-4a1e-8dce-d634922358a1",
            "created_at": "2021-06-10T09:09:47+0000",
            "amount_currency": "EUR",
            "amount": "100.00000000",
            "exchange_rate": "29904.69590000",
            "btc_amount_currency": "BTC",
            "btc_amount": "0.00334396"
        },
        {
            "id": "2192b569-caa8-492a-bc05-5117b594d017",
            "created_at": "2021-06-10T09:02:46+0000",
            "amount_currency": "USD",
            "amount": "2300.00000000",
            "exchange_rate": "36423.21550000",
            "btc_amount_currency": "BTC",
            "btc_amount": "0.06314654"
        },
        {
            "id": "cf53acee-9ed4-4150-9846-eef98be18bb3",
            "created_at": "2021-06-10T09:02:45+0000",
            "amount_currency": "USD",
            "amount": "2300.00000000",
            "exchange_rate": "36423.21550000",
            "btc_amount_currency": "BTC",
            "btc_amount": "0.06314654"
        }
    ]
}
```
