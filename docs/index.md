# xapo-assignment

[![Built with](https://img.shields.io/badge/Built_with-Cookiecutter_Django_Rest-F7B633.svg)](https://github.com/agconti/cookiecutter-django-rest)

API that allows its clients to manage BTC buy orders.

[Documentation Gitlab](https://gitlab.com/amaralunao/xapo-assignment/-/blob/master/docs/index.md)

Powered by [CoinDesk](https://www.coindesk.com/price/bitcoin)

## Prerequisites

- [Docker](https://docs.docker.com/docker-for-mac/install/)  

## Local Development


### Create local env file

- In the project root directory of the project create a `.env` file and add a value for `DJANGO_SECRET_KEY`
  and optionally for `IDEMPOTENCY_TIMEOUT`
- Alternatively you can copy the contents of the `.env.example` file to a `.env` file in the root directory of the project

example:
```
DJANGO_SECRET_KEY = "the_secret_is_out"
IDEMPOTENCY_TIMEOUT = 30
```

### Start the dev server for local development
```bash
docker-compose up
```

### Run a command inside the docker container

```bash
docker-compose run --rm web [command]
```

### Run tests

```bash
docker-compose run --rm web ./manage.py test
```
### Documentation can be viewed locally

http://127.0.0.1:8001/

## How to test the API using curl

- Included `| python -m json.tool` in the instructions for pretty printing. This may be omitted from commands if python not installed

### Creating a buy order

- For instructions on how to generate the value of the header `X-Idempotency-Key`, please see [Orders Doc Gitlab](https://gitlab.com/amaralunao/xapo-assignment/-/blob/master/docs/api/orders.md) or [Orders Doc Local](/api/orders/)


```bash
curl -X POST http://127.0.0.1:8000/api/v1/orders/ -H "Content-Type: application/json" -H "X-Idempotency-Key: 0aba3e53-772d-470b-93b6-ea6096ce467b" -d '{"amount":"100", "amount_currency":"EUR"}' | python -m json.tool

```

### Retrieving the buy orders saved in the database

```bash
curl -X GET http://127.0.0.1:8000/api/v1/list-orders/ | python -m json.tool
```
