from decimal import Decimal, InvalidOperation

from django.conf import settings
from django.db import transaction
from djmoney.contrib.exchange.exceptions import MissingRate
from djmoney.contrib.exchange.models import get_rate
from djmoney.money import Money
from orders.core.exchange import CoindeskExchangeBackend
from rest_framework import serializers

from .exceptions import GenericAPIException
from .models import Order, Wallet


class OrderSerializer(serializers.ModelSerializer):
    """
    A serializer class for Order

    Attributes
    ----------
    all attributes of the Order model
    """
    class Meta:
        model = Order
        fields = '__all__'


class CreateOrderSerializer(serializers.ModelSerializer):
    """
    A serializer class to validate fiat amount and currency and create Order

    Attributes
    ----------
    all attributes of the Order model


    Methods
    -------
    create(validated_data)
       retrieves the exchange rate,
       calls the create_order_and_update_wallet_with_bitcoin_amount method,
       returns an instance of Order

    __init__(*args, **kwargs)
        extends class __init__ method to add extra validation

    _get_rate(currency)
      retrieves BTC to fiat exchange_rate

    """

    class Meta:
        model = Order
        fields = '__all__'
        read_only_fields = ['id', 'exchange_rate', 'btc_amount', 'created_at']

    def __init__(self, *args, **kwargs):
        """
        Extends __init__ method of serializer

        Adds extra validation to the amount and amount_currency fields
        Correct currency code but in lower case also raises ValidationError

        Raises
        ------
        ValidationError, InvalidOperation

        """

        if kwargs.get('data', {}).get('amount_currency', '') not in settings.INPUT_CURRENCIES:
            raise serializers.ValidationError('Please input a valid currency')
        try:
            Decimal(kwargs.get('data', {}).get('amount', ''))
        except InvalidOperation:
            raise serializers.ValidationError('Please input a valid amount')
        super().__init__(*args, **kwargs)

    def create(self, validated_data):
        """
        Immplements serializer create method

        retrieves the exchange rate by calling _get_rate,
        calls the create_order_and_update_wallet_with_bitcoin_amount method,
        returns an instance of Order

        Parameters
        ----------
        validated_data : dict , required
           dictionary of the serializer validated data

        Returns
        -------
        Order instance

        """
        amount = validated_data['amount']
        currency = amount.currency.code
        exchange_rate = self._get_rate(currency)
        btc_amount = Money(amount.amount / exchange_rate, 'BTC')
        order = create_order_and_update_wallet_with_btc_amount(name='Wallet',
                                                               btc_amount=btc_amount,
                                                               exchange_rate=exchange_rate,
                                                               amount=amount)
        return order

    @staticmethod
    def _get_rate(currency):

        """
        Retrieves the exchange_rate
        Calls the CoindeskExchangeBackend.update_rates method
        to refresh exchange_rates

        Calls the get_rate method to retrieve BTC
        to currency exchange_rate

        Parameters
        ----------
        currency : str
            currency code of fiat that needs to be converted

        Returns
        -------
        exchange_rate : Decimal instance
            decimal value of the exchange rate from BTC to fiat currency

        Raises
        ------
        GenericAPIException
            raised in case of MissingRate error - can occur when update_rates
            call is unsuccessful

        """
        CoindeskExchangeBackend().update_rates(base_currency='BTC')
        try:
            exchange_rate = get_rate('BTC', currency, backend=CoindeskExchangeBackend.name)
            return exchange_rate
        except MissingRate:
            raise GenericAPIException(status_code=503,
                                      detail='Service temporarily unavailable, try again later.')


@transaction.atomic
def create_order_and_update_wallet_with_btc_amount(name, btc_amount, exchange_rate, amount):
    """
    Creates or retrieves a Wallet instance

    Updates total_btc_amount value of Wallet instance;
    select_for_update method makes sure the wallet is
    locked for update by other order attempts.

    Creates and returns Order instance
    Makes sure that wallet is updated and order is created as part of
    the same db transaction.

    Parameters
    ----------
    name : str
        name field value of Wallet instance to be retrieved or created
    amount : Money instance
        value of fiat amount and fiat currency;
    exchange_rate : decimal field
        value of the exchange rate at which the BTC was bought
    btc_amount : Money instance
        represents the amount of BTC bought, amount_currency is BTC

    Returns
    -------
    Order instance

    Raises
    ------
    GenericAPIException
        raised in case of ValueError from calling add_btc on the Wallet instance

    """

    wallet = (
        Wallet.objects.select_for_update().get_or_create(name=name))[0]
    try:
        wallet.add_btc(btc_amount)
        order = Order.objects.create(amount=amount,
                                     exchange_rate=exchange_rate,
                                     btc_amount=btc_amount)
    except ValueError as e:
        raise GenericAPIException(detail=str(e))
    return order
