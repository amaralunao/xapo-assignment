import logging

from django.conf import settings
from django.core.cache import cache
from orders.core.exceptions import GenericAPIException
from rest_framework import permissions

logger = logging.getLogger(__name__)


class IsIdempotent(permissions.BasePermission):
    """ Partially implements idempotency requirements

    - Only applies to POST requests
    - If Header X-Idempotency-Key is not sent with POST request,
    then a HTTP_400_BAD_REQUEST Response is returned
    - If Header X-Idempotency-Key is being reused, either with same payload or
    different payload, a HTT_409_CONFLICT Response is being returned
    - There is a cache timeout after which X-Idempotency-Key Header value may be reused.
    - UUID format is not being enforced / validated
    """
    message = 'Duplicate request detected.'

    def has_permission(self, request, view):
        if request.method != 'POST':
            return True
        idempotency_key_value = request.META.get('HTTP_X_IDEMPOTENCY_KEY')
        if idempotency_key_value is None:
            raise GenericAPIException(detail="https://tools.ietf.org/id/draft-idempotency-header-01.html",
                                      status_code=400)
        idempotency_key_value = idempotency_key_value[:128]  # in order to not exceed max cahe key value
        key = 'idemp-{}-{}'.format(request.user.pk, idempotency_key_value)
        is_idempotent = bool(cache.add(key, 'yes',
                                       settings.IDEMPOTENCY_TIMEOUT))
        if not is_idempotent:
            logger.info(u'Duplicate request (non-idempotent): %s', key)
            raise GenericAPIException(detail="https://tools.ietf.org/id/draft-idempotency-header-01.html",
                                      status_code=409)
        return is_idempotent
