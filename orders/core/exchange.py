from django.conf import settings
from djmoney.contrib.exchange.backends.base import BaseExchangeBackend


class CoindeskExchangeBackend(BaseExchangeBackend):
    """ Implements own exchage backend as per documentation:
        https://github.com/django-money/django-money
    """

    name = 'coindesk.com'
    url = settings.COINDESK_URL

    def __init__(self, url=settings.COINDESK_URL):
        self.url = url

    def get_rates(self, **params):
        response = self.get_response(**params)
        bpi = self.parse_json(response)['bpi']
        rates = dict()
        rates['USD'] = bpi['USD']['rate_float']
        rates['EUR'] = bpi['EUR']['rate_float']
        rates['GBP'] = bpi['GBP']['rate_float']
        return rates
