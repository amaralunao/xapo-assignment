import decimal
import uuid

from django.conf import settings
from django.db import models
from djmoney.models.fields import MoneyField
from djmoney.models.validators import MaxMoneyValidator, MinMoneyValidator
from djmoney.money import Money


class Order(models.Model):
    """
    A class used to store the BTC order.

    Attributes
    ----------

    id : uuid field
        field used as unique identifier of the order
    created_at : datetime  field
        field that stores order creation date and time
    amount : money field
        field used to store the fiat amount and fiat currency;
        validation present for min and max fiat currency amount
    exchange_rate : decimal field
        field used to store the exchange rate at which the BTC was bought
    btc_amount : money field
        field used to store amount of BTC bought, currency is BTC by default
    """

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    created_at = models.DateTimeField(auto_now_add=True, db_index=True)
    amount = MoneyField(
        max_digits=18,
        decimal_places=8,
        currency_choices=settings.CURRENCY_CHOICES,
        validators=[
            MinMoneyValidator(0),
            MaxMoneyValidator(999999999),
        ]
    )
    exchange_rate = models.DecimalField(max_digits=16, decimal_places=8)
    btc_amount = MoneyField(
        max_digits=16,
        decimal_places=8,
        default_currency='BTC')

    class Meta:
        ordering = ['-created_at']


class Wallet(models.Model):
    """
    A class used to store total BTC amount in the database

    Attributes
    ----------

    id : uuid field
        field used as unique identifier of the wallet
    created_at : datetime  field
        field that stores order creation date and time
    updated_at : datetime field
        field that stores wallet update date and time
    total_btc_amount : decimal field
        field used to store total amount of BTC bought

    Methods
    -------
    add_btc(btc_amount)
        increments total_btc_amount of instance with btc_amount

    """

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    total_btc_amount = MoneyField(
        max_digits=16,
        decimal_places=8,
        default=Money(decimal.Decimal('0'), 'BTC'))

    def add_btc(self, btc_amount):

        """Increments total_btc_amount of instance with btc_amount.

        Parameters
        ----------
        btc_amount : Money object , required
            The btc_amount to be added

        Raises
        ------
        ValueError
            if total_btc_amount + btc_amount exceeds
            settings.MAXIMUM_BTC_AMOUNT
        """
        if self.total_btc_amount + btc_amount > settings.MAXIMUM_BTC_AMOUNT:
            raise ValueError('Maximum BTC amount exceeded')

        self.total_btc_amount += btc_amount
        self.save()
