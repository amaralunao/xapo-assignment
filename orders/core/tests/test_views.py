import decimal
import uuid

from django.test import override_settings
from django.urls import reverse
from djmoney.money import Money
from orders.core.models import Order
from rest_framework import status
from rest_framework.test import APITestCase


class TestCreateOrderView(APITestCase):
    """
    Test order creation view
    """

    def setUp(self):
        self.url = reverse('order-list')
        self.data = {
            'amount': 100,
            'amount_currency': 'EUR'
        }

    def test_create_order_success(self):
        key = str(uuid.uuid4())
        self.client.credentials(HTTP_X_IDEMPOTENCY_KEY=key)
        response = self.client.post(self.url,
                                    self.data,
                                    format='json')
        assert response.status_code == status.HTTP_201_CREATED
        assert Order.objects.count() == 1

    def test_create_order_bad_request_if_no_idempotency_key_header(self):
        response = self.client.post(self.url,
                                    self.data,
                                    format='json')
        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert Order.objects.count() == 0

    @override_settings(IDEMPOTENCY_TIMEOUT=60)  # Fix the timeout for the test
    def test_create_order_conflict_due_to_same_idempotency_key_used(self):
        key = str(uuid.uuid4())
        self.client.credentials(HTTP_X_IDEMPOTENCY_KEY=key)
        response = self.client.post(self.url,
                                    self.data,
                                    format='json')
        assert response.status_code == status.HTTP_201_CREATED
        assert Order.objects.count() == 1
        response2 = self.client.post(self.url,
                                     self.data,
                                     format='json')
        assert response2.status_code == status.HTTP_409_CONFLICT
        assert Order.objects.count() == 1  # second order not created


class TestOrderView(APITestCase):

    """
    Test order list view
    """

    def setUp(self):
        self.url = reverse('orders-list')
        self.amount = Money(decimal.Decimal('100.00000000'), 'EUR')
        self.exchange_rate = decimal.Decimal('3676.96703000')
        self.btc_amount = Money(decimal.Decimal('0.00271983'), 'BTC')

    def test_list_orders_success(self):
        order1 = Order.objects.create(
            amount=self.amount,
            exchange_rate=self.exchange_rate,
            btc_amount=self.btc_amount)
        order2 = Order.objects.create(
            amount=self.amount,
            exchange_rate=self.exchange_rate,
            btc_amount=self.btc_amount)
        order3 = Order.objects.create(
            amount=self.amount,
            exchange_rate=self.exchange_rate,
            btc_amount=self.btc_amount)

        assert Order.objects.count() == 3

        response = self.client.get(self.url)
        assert response.status_code == status.HTTP_200_OK
        assert response.data['count'] == 3
        # Response is paginated but limit of objects per page is 10
        assert response.data['next'] is None
        assert response.data['previous'] is None
        # Results are shown in reverse chronological order
        assert response.data['results'][0]['id'] == str(order3.id)
        assert response.data['results'][1]['id'] == str(order2.id)
        assert response.data['results'][2]['id'] == str(order1.id)
