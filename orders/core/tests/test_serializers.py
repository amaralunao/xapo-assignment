from decimal import Decimal
from unittest.mock import patch

from django.test import TestCase
from djmoney.contrib.exchange.exceptions import MissingRate
from djmoney.money import Money
from orders.core import models
from orders.core.exceptions import GenericAPIException
from orders.core.exchange import CoindeskExchangeBackend
from orders.core.models import Wallet
from orders.core.serializers import (
    CreateOrderSerializer, create_order_and_update_wallet_with_btc_amount)
from rest_framework import serializers


class TestCreateOrderSerializer(TestCase):

    def setUp(self):
        self.valid_data = {'amount': '100',
                           'amount_currency': 'EUR'}

    @patch('orders.core.serializers.CreateOrderSerializer._get_rate')
    @patch('orders.core.serializers.create_order_and_update_wallet_with_btc_amount')
    def test_create_order_success(self,
                                  mock_create_order_and_update_wallet,
                                  mock_get_rate):
        serializer = CreateOrderSerializer(data=self.valid_data)
        assert serializer.is_valid()
        mock_get_rate.return_value = Decimal('23232.89')

        serializer.create(serializer.validated_data)

        mock_get_rate.assert_called_once_with('EUR')
        mock_create_order_and_update_wallet.assert_called_once()

    @patch('orders.core.serializers.CreateOrderSerializer._get_rate')
    def test_create_order_successful_save_to_database(self, mock_get_rate):
        serializer = CreateOrderSerializer(data=self.valid_data)
        assert serializer.is_valid()
        mock_get_rate.return_value = Decimal('23232.89')
        serializer.create(serializer.validated_data)
        assert models.Order.objects.count() == 1
        assert models.Wallet.objects.count() == 1
        assert models.Wallet.objects.all()[0].total_btc_amount.amount > Decimal('0')

    def test_serializer_raises_validation_error_when_amount_invalid(self):
        invalid_data = {'amount': 'foobar',
                        'amount_currency': 'USD'}

        with self.assertRaises(serializers.ValidationError, msg='Please input a valid amount'):
            CreateOrderSerializer(data=invalid_data)

    def test_serializer_raises_validation_error_when_currency_invalid(self):
        invalid_data = {'amount': '100',
                        'amount_currency': 'BLA'}

        with self.assertRaises(serializers.ValidationError, msg='Please input a valid currency'):
            CreateOrderSerializer(data=invalid_data)

    def test_serializer_raises_validation_error_when_fiat_amount_exceeds_maximum_value(self):
        invalid_data = {'amount': '1000000000',
                        'amount_currency': 'USD'}

        serializer = CreateOrderSerializer(data=invalid_data)
        assert not serializer.is_valid()
        assert serializer.errors['amount']

    def test_serializer_raises_validation_error_when_fiat_amount_negative(self):
        invalid_data = {'amount': '-100',
                        'amount_currency': 'USD'}

        serializer = CreateOrderSerializer(data=invalid_data)
        assert not serializer.is_valid()
        assert serializer.errors['amount']

    @patch('orders.core.serializers.CoindeskExchangeBackend.update_rates')
    @patch('orders.core.serializers.get_rate')
    def test_get_rate_success(self, mock_get_rate, mock_update_rates):
        mock_get_rate.return_value = Decimal('27621.766000')
        serializer = CreateOrderSerializer(data=self.valid_data)
        assert serializer._get_rate('GBP') == Decimal('27621.766000')
        mock_update_rates.assert_called_once()
        mock_get_rate.assert_called_once_with('BTC',
                                              'GBP',
                                              backend=CoindeskExchangeBackend.name)

    @patch('orders.core.serializers.CoindeskExchangeBackend.update_rates')
    @patch('orders.core.serializers.get_rate')
    def test_get_rate_raises_serviceunavailable_exception(self, mock_get_rate, mock_update_rates):
        mock_get_rate.side_effect = MissingRate()
        serializer = CreateOrderSerializer(data=self.valid_data)
        with self.assertRaises(GenericAPIException,
                               msg='Service temporarily unavailable, try again later.'):
            serializer._get_rate('GBP')
        mock_update_rates.assert_called_once()
        mock_get_rate.assert_called_once_with('BTC',
                                              'GBP',
                                              backend=CoindeskExchangeBackend.name)

    def test_create_order_and_update_wallet_with_btc_amount(self):
        btc_amount = Money(Decimal('0.12345678'), 'BTC')
        amount = Money(Decimal('100'), 'EUR')
        exchange_rate = Decimal('27621.766000')
        name = "TestWallet"
        with patch.object(Wallet, 'add_btc', return_value=None) as mock_add_btc:

            create_order_and_update_wallet_with_btc_amount(name=name,
                                                           exchange_rate=exchange_rate,
                                                           btc_amount=btc_amount,
                                                           amount=amount)
            mock_add_btc.assert_called_once_with(btc_amount)
            assert models.Order.objects.count() == 1
            assert models.Wallet.objects.count() == 1

    def test_create_order_and_update_wallet_with_btc_amount_raises_exception(self):
        btc_amount = Money(Decimal('0.12345678'), 'BTC')
        amount = Money(Decimal('100'), 'EUR')
        exchange_rate = Decimal('27621.766000')
        name = "TestWallet"

        with patch.object(Wallet, 'add_btc', side_effect=ValueError) as mock_add_btc:

            with self.assertRaises(GenericAPIException, msg='Maximum BTC amount exceeded'):
                create_order_and_update_wallet_with_btc_amount(name=name,
                                                               exchange_rate=exchange_rate,
                                                               btc_amount=btc_amount,
                                                               amount=amount)
            mock_add_btc.assert_called_once_with(btc_amount)
            assert models.Order.objects.count() == 0
            assert models.Wallet.objects.count() == 0
