import decimal

from django.conf import settings
from django.test import TestCase
from djmoney.money import Money
from orders.core import models


class TestWalletModel(TestCase):

    def setUp(self):
        self.wallet = models.Wallet.objects.create(name="Wallet")

    def test_add_btc_to_newly_created_instance_successful(self):
        new_wallet = models.Wallet.objects.create(name="New Wallet")
        btc_amount = Money(decimal.Decimal('0.00323456'), 'BTC')
        new_wallet.add_btc(btc_amount)
        assert new_wallet.total_btc_amount.amount == btc_amount.amount

    def test_increment_btc_successful(self):
        btc_amount = Money(decimal.Decimal('0.00323456'), 'BTC')
        new_btc_amount = Money(decimal.Decimal('0.00323456'), 'BTC')
        self.wallet.add_btc(btc_amount)
        assert self.wallet.total_btc_amount.amount == btc_amount.amount
        self.wallet.add_btc(new_btc_amount)
        final_amount = btc_amount + new_btc_amount
        assert self.wallet.total_btc_amount.amount == final_amount.amount

    def test_increment_btc_raises_value_error_maximum_btc_exceeded(self):
        new_btc_amount = settings.MAXIMUM_BTC_AMOUNT + Money(decimal.Decimal('0.00000001'), 'BTC')
        assert self.wallet.total_btc_amount.amount == decimal.Decimal('0')
        with self.assertRaises(ValueError, msg='Maximum BTC amount exceeded'):
            self.wallet.add_btc(new_btc_amount)
