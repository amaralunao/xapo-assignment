from rest_framework import mixins, viewsets
from rest_framework.permissions import AllowAny

from .models import Order
from .permissions import IsIdempotent
from .serializers import CreateOrderSerializer, OrderSerializer


class CreateOrderViewSet(mixins.CreateModelMixin,
                         viewsets.GenericViewSet):
    """
    Creates Order
    """
    queryset = Order.objects.all()
    serializer_class = CreateOrderSerializer
    permission_classes = [AllowAny, IsIdempotent]

class OrderViewSet(mixins.ListModelMixin,
                   viewsets.GenericViewSet):

    """
    Lists created orders
    """

    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    permission_classes = [AllowAny]
